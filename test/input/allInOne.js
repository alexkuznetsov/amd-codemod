define("module/a", [], function() {
    var one = foo("one");
    var two = bar.baz("two");
    return one;
});

require(["module/a", "module/b"], function(a, b) {
    foobar("loaded!", a, b, somedep);
});

define("module/b", ["require"], function(require) {
    var a = require("module/a");
    var b = a + bar;
    return b;
});
