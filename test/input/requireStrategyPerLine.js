define("test/input/requireStrategyPerLine", ["require"], function(require) {
    var foo = require("f/o/o");
    // require() lines should show above this comment.
    foo.bar();
    bar.baz();
    baz.wtf();
    bazfoo(bar, baz);
});
