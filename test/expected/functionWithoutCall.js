define("test/input/functionWithoutCall", ["bar/baz", "foo/bar"], function(Barbaz, foobar) {
    Something.setCallback(Barbaz);

    foobar.apply(this, {});
});
