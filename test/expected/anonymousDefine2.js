define(["bar/baz", "foobar/foofoo", "jquery"], function(Barbaz, FOO, jQuery) {
    var url = jQuery("#main").href();
    jQuery("#main").data('link', Barbaz());
    jQuery.get(url, {}).then(FOO.bar);
});
