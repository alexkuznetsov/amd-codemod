define("test/input/parameterWithSameNameExists", ["some/other/foo", "b/a/r"], function(foo, bar) {
    foo.bar();

    bar.baz();
});
