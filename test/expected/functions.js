define('test/data/function', ["baz/foo", "bar/baz", "foo/bar"], function(Bazfoo, Barbaz, foobar) {
    foobar(Barbaz(true), Bazfoo(new Exception("yolo")), true);

    Leave.me.alone();

    Bazfoo({}, [], "", false).each(function(x,y) {
        Barbaz(x).get(y).init();

        Cant.touch.this();
    });
});
