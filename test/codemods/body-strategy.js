"use strict";
module.exports = function(fileInfo, api) {
    var j = api.jscodeshift;
    var plugins = require("../../index");
    var src = j(fileInfo.source);

    var defineBlocks = src.getAMDDefineCalls()

    defineBlocks.forEach(function(path) {
        var code = j(path);
        code.toBodyStrategy();
    });

    return src.toSource();
};
