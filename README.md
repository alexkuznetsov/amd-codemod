# AMD Transformer with jscodeshift

This transformer module converts JS into AMDified JS using jscodeshift.

[![build-status](https://bitbucket-badges.useast.atlassian.io/badge/atlassian/amd-codemod.svg)](https://bitbucket.org/atlassian/amd-codemod/addon/pipelines/home)

## Install

Install jscodeshift globally:

`npm install -g jscodeshift@0.3.13`

Create a new folder:

`mkdir transform-amd && cd transform-amd`

Install `amd-codemod` in the folder:

`npm install bitbucket:atlassian/amd-codemod`

## Usage

Create a file `transform-amd.js` in the folder, from the following template:

```javascript
module.exports = function(fileInfo, api) {
    var jscodeshift = api.jscodeshift;
    var amd = require('amd-codemod/index')(jscodeshift);
    var src = jscodeshift(fileInfo.source);
    return src
        .getAMDCalls()
        .forEach(function(path) {
            var code = jscodeshift(path);
            return code
                /* BEGIN add replaces here */
                //.toAMD("foo", "f/o/o", "amdFoo")
                //.toAMD("foobar", "foo/bar", "amdBar")
                /* END add replaces here */
                .toSource();
        })
        .toSource();
};
```

Be sure to add your AMD modules to the list before you proceed. 
See the API docs below for more information.

### Run with jscodeshift

Perform a dry run using the `-d` parameter:

`jscodeshift -d -t transform-amd.js /dir/with/files/to/convert`

If you are satisfied, run it:

`jscodeshift -t transform-amd.js /dir/with/files/to/convert`

See [jscodeshift documentation](https://github.com/facebook/jscodeshift) for more options.


## Transform methods

### jscodeshift.toAMD(name, module, properName, newInstanceMemberName)

Replaces the occurence of `name` with the AMD module `module` on the provided path.

#### name
Type: `String`
Required: yes

Name of the variable or function to look for. You can provide several formats:

* A single Identifier value - i.e., a string with only alpha-numeric characters.
* A nested static member value -- e.g., `"MyGlobal.Foo.doStuff"`.
* A nested instance member value -- e.g., `"MyGlobal.Foo#doStuff"`.

See the Examples section for more detail.

#### moduleName
Type: `String`
Required: yes

Name of the AMD module.

#### properName
Type: `String`
Required: no

Name of the replacement variable. If not set, `name` is used.

If you are replacing anything other than an Identifier, you need to provide this,
otherwise the variable will be invalid JavaScript syntax.
 
#### newInstanceMemberName
Type: `String`
Required: no
 
Name of the new instance member, can only be used if you have provided a nested instance member value for `name` such
 as `"MyGlobal.Foo#doStuff"`. The value of `newInstanceMemberName` will replace `doStuff`.

### jscodeshift.toBodyStrategy()

If any module dependencies are listed in the array parameter of the `define()` call,
they are moved in to the body of the module's factory function instead, and imported
via a local `require` function.

This method is intended for reformatting files with long lists of module dependencies
so they are easier to read and maintain.

Note that modules using the resulting body strategy syntax *must* be run through
an AMD optimiser such as [r.js](https://github.com/requirejs/r.js), otherwise
a module's dependencies may be understood incorrectly by an AMD loader.


## Filtering methods

### jscodeshift.getAMDDefineCalls()

Returns a collection of paths for `define()` AMD function calls.

### jscodeshift.getAMDRequireCalls(includeLocalRequires)

Returns a collection of paths for various `require()` AMD function calls.

#### includeLocalRequires
Type: `boolean`
Required: no

If set to `true`, the function will return all usages of `require()`, including
those found inside non-global scopes, such as inside a module's factory function body.

If set to `false` or omitted, the function will only return top-level `require()` calls.

### jscodeshift.getAMDCalls()

Returns a collection of all top-level `define()` and `require()` AMD function calls.

### amd.isAMDDefine(path)

Validates whether the `path` contains the `define()` method, indicating it is an AMD module.

#### path
Type: `Object`
Required: yes

An AST node.


## Examples

The `toAMD` function allows you to replace deeply-nested values with AMD module dependencies.

When your codemod declares a nested static member value, the entire value will be replaced
with a module reference.

```javascript
// input.js
define("my/module", [], function() {
    MyGlobal.Foo.doStuff("a", 1);
});

// if the transform-amd.js file includes this line in the transform:
code.toAMD("MyGlobal.Foo.doStuff", "foo/doStuff", "doStuff")

// the entire nested chain will be replaced in output.js:
define("my/module", ["foo/doStuff"], function(doStuff) {
    doStuff("a", 1);
});
```

When your codemod declares a nested *instance* member value, everything up to the hash
(`#`) will be replaced with the module dependency, and everything after the hash
will stay in place as a usage of a member value of the module.

```javascript
// input.js
define("my/module", [], function() {
    MyGlobal.Foo.doStuff("a", 1);
});

// if the transform-amd.js file includes this line in the transform:
code.toAMD("MyGlobal.Foo#doStuff", "foo", "amdFoo")

// only the namespace will be replaced in output.js, leaving the instance member intact:
define("my/module", ["foo"], function(amdFoo) {
    amdFoo.doStuff("a", 1);
});
```

The instance member format allows you to select specific values present
in a namespace that you want to move to specific modules, instead of moving
everything on to a single monolithic module.

```javascript
// input.js
define("my/module", [], function() {
    MyGlobal.Foo.doStuff("a", 1);
    MyGlobal.Foo.bar = "baz";
    MyGlobal.Foo.waitFor(function() { });
});

// if the transform-amd.js file includes this line in the transform:
code
    .toAMD("MyGlobal.Foo#waitFor", "events", "events")
    .toAMD("MyGlobal.Foo", "my/foo", "amdFoo")

// the entire nested chain will be replaced in output.js:
define("my/module", ["my/foo", "events"], function(amdFoo, events) {
    amdFoo.doStuff("a", 1);
    amdFoo.bar = "baz";
    events.waitFor(function() { });
});
```

## Caveats

This tool exists to aid in code refactor, but a human should **always** check the results.
There are a few caveats with the current implementation.

### Duplicate parameters in 'body' function

If you define a `.toAMD("_", "underscore")`, and a module already has a 
dependency on a *different* `_` (i.e. "libs/underscore-2.3.0"),
the tool will add another `_` to your body function:

```javascript
define("my/module", ["libs/underscore-2.3.0", "underscore"], function(_, _) {
    // ...
});
```

Which yields invalid code in strict mode.

### Local variables/functions with the same name

If you define a `.toAMD("foo", "foo/bar")` and a module contains a 
locally declared variable "foo", the tool will overwrite the local variable
and add the AMD module as a dependency.

## Debugging

To enable debugging, change the first line in `jscodeshift.sh` to:

```# !/usr/bin/env node debug --stack-trace-limit=1000```

Be sure to use `--run-in-band` (see above) to run all in one thread.

