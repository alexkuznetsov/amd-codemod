"use strict";
const NodeUtil = require('../util/node-util');

module.exports = j => {
    const utils = new NodeUtil(j);

    function isAMDDefine(path) {
        return utils.isCallOf('define')(path);
    }

    function isAMDRequire(path) {
        return utils.isCallOf('require')(path);
    }

    function getAMDDefineCalls() {
        return this.find(j.CallExpression).filter(isAMDDefine);
    }

    function getAMDRequireCalls(includeLocalRequires) {
        let calls = this.find(j.CallExpression).filter(isAMDRequire);
        if (!includeLocalRequires) {
            calls = calls.filter(function(p) {
                return p.scope.isGlobal;
            });
        }
        return calls;
    }

    function getAMDCalls() {
        return this.find(j.CallExpression).filter(function(path) {
            return path.scope.isGlobal && (isAMDDefine(path) || isAMDRequire(path));
        });
    }

    j.registerMethods({
        getAMDCalls,
        getAMDDefineCalls,
        getAMDRequireCalls
    });
}
