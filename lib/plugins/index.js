'use strict';

function tryRegister(path, j) {
    try {
        require(path)(j);
    } catch (e) {
        // probably already registered
    } 
}

module.exports = j => {
    tryRegister('./collection-utils', j);
    tryRegister("./get-amd-calls", j);
    tryRegister("./strict", j);
}