"use strict";
const types = require("ast-types").namedTypes;

module.exports = function (j, node) {
    const { deps, factory } = extractDeps(node);

    function isFunction(node) {
        return types.FunctionExpression.check(node) || types.ArrowFunctionExpression.check(node);
    }

    function extractDeps(node) {
        let deps;
        let factory;

        // define(fn)
        if (node.arguments.length === 1
            && isFunction(node.arguments[0])
        ) {
            return {
                deps: j.arrayExpression([]),
                factory: node.arguments[0]
            }
        }

        // define('name', fn)
        if (node.arguments.length === 2
            && types.Literal.check(node.arguments[0])
            && isFunction(node.arguments[1])
        ) {
            return {
                deps: j.arrayExpression([]),
                factory: node.arguments[1]
            }
        }

        // define([deps], fn)
        if (node.arguments.length === 2
            && types.ArrayExpression.check(node.arguments[0])
            && isFunction(node.arguments[1])
        ) {
            return {
                deps: node.arguments[0],
                factory: node.arguments[1]
            }
        }

        // define('name', [deps], fn)
        if (node.arguments.length === 3
            && types.Literal.check(node.arguments[0])
            && types.ArrayExpression.check(node.arguments[1])
            && isFunction(node.arguments[2])
        ) {
            return {
                deps: node.arguments[1],
                factory: node.arguments[2]
            }
        }

        throw new Error("Don't know how to parse this define/require");
    }

    function ensureAMDDependencies() {
        var idx = node.arguments.indexOf(deps);
        if (idx > -1) return;

        var depsPosition = node.arguments.indexOf(factory);
        node.arguments.splice(depsPosition, 0, deps);
    }

    function removeAMDDependencies() {
        var depsPosition = node.arguments.indexOf(deps);
        if (depsPosition > -1) node.arguments.splice(depsPosition, 1);
    }

    function addDependency(argName, moduleName) {
        ensureAMDDependencies();
        deps.elements.splice(0, 0, j.literal(moduleName));
        factory.params.splice(0, 0, j.identifier(argName));
    }

    function removeDependencies() {
        deps.elements.length = 0;
        factory.params.length = 0;
        removeAMDDependencies()
    }

    function hasDependency(moduleName) {
        return j(deps.elements)
            .has(path => types.Literal.check(path.node) && path.node.value === moduleName)
    }

    function getDependencies() {
        return deps.elements.slice(0, factory.params.length).map((moduleName, index) => ({
            module: moduleName.value,
            argument: factory.params[index].name
        }));
    }

    function getDependency(moduleName) {
        var dep = getDependencies().find(dep => dep.module == moduleName);
        if (!dep) throw new Error(`Dependency ${moduleName} not found`);

        return dep.argument;
    }

    return {
        addDependency,
        hasDependency,
        getDependency,

        getDependencies,
        removeDependencies
    }
};
