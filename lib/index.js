'use strict';

var InjectDependencyTransform = require('./transforms/inject-dependency');
var ToBodyStrategyTransform = require('./transforms/to-body-strategy');
var NodeUtil = require('./util/node-util');

const plugins = require('./plugins');
/**
 *
 * @param {Object} j instance of jscodeshift
 * @returns {{}}
 */
function registerMethods(j) {
    var module = {};
    var injectDepsTransform = new InjectDependencyTransform(j);
    var toBodyStratTransform = new ToBodyStrategyTransform(j);

    try {
        j.registerMethods({
            toAMD: injectDepsTransform.toAMD,
            toBodyStrategy: toBodyStratTransform.toBodyStrategy
        });
    } catch (e) {
        // Most likely, the methods are already registered
    }

    j.use(plugins);

    return module;
};

module.exports = {
    register: registerMethods
};
