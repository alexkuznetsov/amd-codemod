"use strict";

var bodyStrategy = require("../strategy/body");
var factoryStrategy = require("../strategy/factory");

/**
 *
 * @param {Object} j instance of jscodeshift
 * @returns {{}}
 */
module.exports = function(j) {

    function getDependenciesFromNode(nodeArgs) {
        return nodeArgs.length === 3 ? nodeArgs[1] : j.arrayExpression([]);
    }

    function isBodyStrategy(deps) {
        return deps.elements.length === 1 && deps.elements[0].value === "require";
    }

    function toBodyStrategy() {
        this.nodes().forEach(function(node) {
            var deps = getDependenciesFromNode(node.arguments);
            if (isBodyStrategy(deps) || deps.elements.length === 0) return;

            var fStrategy = factoryStrategy(j, node);
            var bStrategy = bodyStrategy(j, node);
            fStrategy.getDependencies().forEach(function(dep) {
                bStrategy.addDependency(dep.argument, dep.module);
            });
            fStrategy.removeDependencies();
            fStrategy.addDependency("require","require");
        }.bind(this));

        return this;
    }

    return {
        toBodyStrategy: toBodyStrategy
    };
};
