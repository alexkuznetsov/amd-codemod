"use strict";

var bodyStrategy = require("../strategy/body");
var factoryStrategy = require("../strategy/factory");
var NodeUtil = require("../util/node-util");
var types = require("ast-types").namedTypes;

/**
 *
 * @param {Object} j instance of jscodeshift
 * @returns {{}}
 */
module.exports = function(j) {
    var utils = new NodeUtil(j);

    function buildStrategy(node) {
        var nodeArgs = node.arguments;
        var dependencies = node.arguments.find(arg => types.ArrayExpression.check(arg));
        if (dependencies && dependencies.elements.length === 1 && dependencies.elements[0].value === "require") {
            return bodyStrategy(j, node);
        }

        return factoryStrategy(j, node);
    }

    function toAMD(name, moduleName, properName, newInstanceMemberName) {
        var findBy = j.MemberExpression;
        var moduleProperty;

        if (!properName) {
            properName = name;
        }
        if (name.split('#').length === 2) {
            name = name.split('#');
            moduleProperty = name[1];
            name = name.join('.');
        }
        if (name.split('.').length === 1) {
            findBy = j.Identifier;
        }

        var replaceUsage = function (path) {
            var completeName;
            if (moduleProperty) {
                var instanceMemberName = (newInstanceMemberName) ? newInstanceMemberName : moduleProperty;
                completeName = [properName, instanceMemberName].join('.')
            } else {
                completeName = properName;
            }
            return j(path).replaceWith(completeName);
        };

        this.nodes().forEach(function(node) {
            var usages = this
                .find(findBy)
                .filter(utils.isUsageOf(name));

            if (usages.size()) {
                // dependencies can be required in two ways:
                // 1) 'factory' - define("name", ["dependency"], function(dependencyName) { ... });
                // 2) 'body' - define("name", ["require"], function(require) { var dependencyName = require("dependency"); }
                var strategy = buildStrategy(node);

                if(strategy.hasDependency(moduleName)) {
                    // Set the replacement name to the existing dependency.
                    properName = strategy.getDependency(moduleName);
                } else {
                    strategy.addDependency(properName || name, moduleName);
                }
            }

            if (name !== properName) {
                usages.forEach(replaceUsage);
            }
        }.bind(this));

        return this;
    }

    return {
        toAMD: toAMD
    };
};
